const mongoose = require('mongoose');
const autoIncrement = require('mongoose-sequence')(mongoose);

const TodoTaskSchema = new mongoose.Schema({
  content: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

TodoTaskSchema.plugin(autoIncrement, { inc_field: 'todo_task_id' });
TodoTaskSchema.index({ name: 'text' });
const TodoTask = mongoose.model('TodoTask', TodoTaskSchema);
module.exports = TodoTask;
