const todoTaskController = require('../../controllers/todo-task.controller');
const todoTaskModel = require('../../models/todo-task.model');
const httpMocks = require('node-mocks-http');
const supertest = require('supertest');
const app = require('../../server');
const mongoose = require('mongoose');

let req, res, next;
todoTaskModel.save = jest.fn();
const newTodo = { content: 'lorem ipsum doner' };

// Connect to the MongoDB Memory Server
beforeAll(async () => {
  await mongoose.connect(
    global.__MONGO_URI__,
    { useNewUrlParser: true, useCreateIndex: true },
    err => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    }
  );
});

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = null;
});

describe('todo-task controller', () => {
  it('should have addTodoTask function defined', () => {
    expect(typeof todoTaskController.addTodoTask).toBeDefined();
  });
  it('should call addTodoTask', async () => {
    req.body = newTodo;
    await todoTaskController.addTodoTask(req, res, next);
    expect(res._getJSONData()).toStrictEqual({
      message: 'New Todo-Task created..!'
    });
    expect(res.statusCode).toBe(201);
    expect(res._isEndCalled()).toBeTruthy();
  });
});
