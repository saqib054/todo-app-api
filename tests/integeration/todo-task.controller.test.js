const todoTaskController = require('../../controllers/todo-task.controller');
const todoTaskModel = require('../../models/todo-task.model');
const httpMocks = require('node-mocks-http');
const supertest = require('supertest');
const app = require('../../server');
const mongoose = require('mongoose');

let req, res, next;
todoTaskModel.save = jest.fn();
const newTodo = { content: 'lorem ipsum doner' };

// Connect to the MongoDB Memory Server
beforeAll(async () => {
  await mongoose.connect(
    global.__MONGO_URI__,
    { useNewUrlParser: true, useCreateIndex: true },
    err => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    }
  );
});

beforeEach(() => {
  req = httpMocks.createRequest();
  res = httpMocks.createResponse();
  next = null;
});

describe('Testing todo router', () => {
  it('tests the add-todo-task route', async () => {
    req.body = newTodo;
    const response = await supertest(app)
      .post('/api/add-todo-task')
      .send(newTodo);

    expect(response.status).toBe(201);
    expect(response.body).toHaveProperty('message');
  });
});
