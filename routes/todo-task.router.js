const express = require('express');
const router = express.Router();

const {
  addTodoTask,
  getTodoTask,
  getAllTodoTasks,
  updateTodoTask,
  deleteTodoTask
} = require('../controllers/todo-task.controller');

// Create new todo task
// @route POST api/add-todo-task
// @access User
router.post('/add-todo-task', addTodoTask);

// Get one todoTask
// @route GET api/todo-task/id
// @access Public
router.get('/todo-task/:id', getTodoTask);

// Get all todo tasks
// @route GET api/todo-task-list
// @access Public
router.get('/todo-task-list', getAllTodoTasks);

// Update a todo-task
// @route PUT api/update-todo-task
// @access Public
router.put('/update-todo-task/:id', updateTodoTask);

// Delete a todo task
// @route DELETE api/delete-todo-task/id
// @access Public
router.delete('/delete-todo-task/:id', deleteTodoTask);

module.exports = router;
