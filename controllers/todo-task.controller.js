const TodoTask = require('../models/todo-task.model');
const { ErrorHandler } = require('../middleware/error');

exports.addTodoTask = async (req, res, next) => {
  try {
    const { content } = req.body;
    if (!content) {
      throw new ErrorHandler(400, 'Content is required');
    }
    const todo = new TodoTask({
      content
    });
    await todo.save();
    res.status(201);
    res.json({ message: 'New Todo-Task created..!' });
  } catch (error) {
    next(error);
  }
};

exports.getTodoTask = async (req, res, next) => {
  try {
    const queryParams = { _id: req.params.id };
    const todoTask = await TodoTask.findOne(queryParams);
    if (!todoTask) {
      throw new ErrorHandler(404, 'Todo-Task not found');
    }
    res.status(200);
    res.json(todoTask);
  } catch (error) {
    next(error);
  }
};

exports.getAllTodoTasks = async (req, res, next) => {
  try {
    const queryParams = {
      pageIndex: parseInt(req.query.pageIndex) || 1,
      size: parseInt(req.query.size) || 5
    };
    const totalCount = await TodoTask.countDocuments();
    const todoTasks = await TodoTask.find()
      .skip(queryParams.size * (queryParams.pageIndex - 1))
      .limit(queryParams.size);
    if (!todoTasks) {
      throw new ErrorHandler(404, 'Todo-Tasks not found');
    }
    res.status(200);
    res.json({ todoTasks, totalCount });
  } catch (error) {
    next(error);
  }
};

exports.updateTodoTask = async (req, res, next) => {
  try {
    const queryParams = { _id: req.params.id };
    const todoTask = await TodoTask.findOne(queryParams);
    if (!todoTask) {
      throw new ErrorHandler(404, 'Todo-Task not found');
    }

    const { content } = req.body;
    if (!content) {
      throw new ErrorHandler(400, 'Content is required');
    }
    const updateTodoTask = {
      $set: { content }
    };
    await TodoTask.updateOne(queryParams, updateTodoTask);
    res.status(200);
    res.json({ message: 'Todo-Task updated..!' });
  } catch (error) {
    next(error);
  }
};

exports.deleteTodoTask = async (req, res, next) => {
  try {
    const queryParams = { _id: req.params.id };
    const todoTask = await TodoTask.findOne(queryParams);
    if (!todoTask) {
      throw new ErrorHandler(404, 'Todo-Task not found');
    }
    await TodoTask.deleteOne(queryParams);
    res.status(200);
    res.json({ message: 'Todo-Task deleted..!' });
  } catch (error) {
    next(error);
  }
};
